<?php
include_once('init.php');
include_once('autoload.php');

$lessonIndex = ArgumentsResolver::getLessonIndex($argv);

try {
	$lessonDir = LessonResolver::getLesson($lessonIndex);
	echo "Let's start lesson: " . LessonResolver::getLesson($lessonIndex) . "\n";
	include_once BASE_DIR . DIRECTORY_SEPARATOR . $lessonDir . DIRECTORY_SEPARATOR . 'index.php';
} catch (RuntimeException $exception) {
	echo "Error: ".$exception->getMessage() . "\n";
}

echo "... end ...\n";