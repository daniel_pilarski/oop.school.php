<?php
class Jar implements Openable {
	private $contents;

	function __construct($contents){
		$this->contents = $contents;
	}

	function open(){
		echo "Jar is opened" . PHP_EOL;
	}

	function close(){
		echo "Jar is closed" . PHP_EOL;
	}
}