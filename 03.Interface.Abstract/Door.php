<?php
class Door implements Openable {
	private $locked = false;
	private $opened = false;

	public function open(){
		if ($this->locked) {
			echo "It's impossible to open the door. It is closed key." . PHP_EOL;
		}	else {
			echo "Door is opening ... skrzyyyyyp! ... Ready." . PHP_EOL;
			$this->opened = true;
		}
	}

	public function close(){
		if ($this->opened) {
			echo "Bummmmm! Trachhh!" . PHP_EOL;
			$this->opened = false;
		}	else {
			echo 'Door is already closed.' . PHP_EOL;
		}
	}

	function lockDoor(){
		$this->locked = true;
		echo 'Locked the door' . PHP_EOL;;
	}

	function unlockDoor(){
		$this->locked = false;
		echo 'Unlocked the door.' . PHP_EOL;;
	}
}