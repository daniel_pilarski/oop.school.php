<?php
function openSomething(Openable $obj){
	if ($obj instanceof Door){
		$obj->unlockDoor();
	}
	$obj->open();
}

function closeSomething(Openable $obj){
	$obj->close();
	if ($obj instanceof Door){
		$obj->lockDoor();
	}
}


$door = new Door();
$jar = new Jar('cherry jam');

openSomething($door);
openSomething($jar);

closeSomething($door);
closeSomething($jar);

$door->open();

// $book = new Book();
// openSomething($book);
// closeSomething($book);