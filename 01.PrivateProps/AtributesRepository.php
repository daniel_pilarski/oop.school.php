<?php
class attributesRepository {
	private $db;
	
	public function __construct()
	{
		$this->db = IoC::get('db');
	}
	
	public function getAttributes()
	{
		$this->db->selectAsAssoc(/*...params of SQL query...*/);
	}
	
	public function getAttribute($id)
	{
		$this->db->selectSingleRowAsAssoc(/*...params of SQL query...*/);
	}
	
}
============================

$r = new attributesRepository();
$r->getAttribute(12);