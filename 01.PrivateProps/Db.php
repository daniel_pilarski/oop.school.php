<?php
class Db {
	private $connection;
	
	public function __construct()
	{
		$dbConfig = $this->getDBConfig();
		$this->connection = new mysqli($dbConfig['host'], $dbConfig['user'], $dbConfig['passwd'], $dbConfig['dbname']);
		
		if ($this->connection->connect_errno){
			throw new Exception('Error of database connection: ' . $this->connection->connect_error);
		}
		else {
			$this->connection->set_charset('utf8');
			mysqli_report(MYSQLI_REPORT_ERROR);
		}
	}
	
	public function selectAsAssoc(/*...params of SQL query...*/){
		$sql = '... some sql statement ...';
		$qryResult = $this->connection->query($sql);
		return $this->prepareResponseFromQueryResult($qryResult);
	}
	
	public function selectSingleRowAsAssoc($fields, $tables, $where='', $order='', $group=''){
		$sql = '... some sql statement ...';
		$qryResult = $this->connection->query($sql);
		return $this->prepareSingleRowResponseFromQueryResult($qryResult);
	}
		
	private function getDBConfig()
	{
		// return configuration e.g. from file or from remote configuration service  
	}
	
	private function prepareResponseFromQueryResult($queryResult)
	{
		$result = array();		
		if ($queryResult) {
			while ($row = $queryResult->fetch_assoc()){
				$result[] = $row;
			}
			$queryResult->close();
		}
		return $result;
	}
	
	private function prepareSingleRowResponseFromQueryResult($queryResult)
	{
		$result = array();		
		if ($queryResult){
			$result = $queryResult->fetch_assoc();
			$queryResult->close();
		}
		return $result;
	}
	}
}