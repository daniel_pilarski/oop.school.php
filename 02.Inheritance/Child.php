<?php
class Child extends Person {
	private $father;
	private $mather;
	
	public function setFather(Adult $father)
	{
		$this->father = $father;
	}
	
	public function setMather(Adult $mather)
	{
		$this->mather = $mather;
	}
	
	public function sayHello()
	{
		$this->introduceMyself();
		$this->introduceFather();
		$this->introduceMather ();
	}
	
	private function introduceMyself() {
		parent::sayHello();
		echo PHP_EOL;
	}
	
	private function introduceMather() {
		printf(
				"My mather is %s %s. She is %d yers old.",
				$this->mather->getName(),
				$this->mather->getSurname(),
				$this->mather->getAge()
		);
		echo PHP_EOL;
	}

	
	private function introduceFather()
	{
		printf(
				"My father is %s %s. He is %d yers old.",
				$this->father->getName(),
				$this->father->getSurname(),
				$this->father->getAge()
		);
		echo PHP_EOL;		
	}
}