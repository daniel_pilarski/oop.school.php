<?php
class Adult extends Person {
	private $children = array();
	
	public function addChild(Child $child)
	{
		$this->children[] = $child;
		$child->setFather($this);
	}
	
	public function sayHello()
	{
		$this->introduceMyself();
		// @ToDo: do introduction of children. 
		// @Hint: Use introduceChildren() method. If there is no children write appropriate message 
		$this->introduceChildren();
	}
	
	private function introduceMyself() {
		parent::sayHello();
		echo PHP_EOL;
	}
	
	private function introduceChildren()
	{
		echo 'My children:' . PHP_EOL;
		foreach ($this->children as $child) {
			printf(" - %s, %d yers old.", $child->getName(), $child->getAge());
			echo PHP_EOL;
		}
	}
}