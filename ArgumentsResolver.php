<?php
class ArgumentsResolver {
	const LESSON_PARAM_NAME = 'lesson'; 
	 
	public static function getAgruments($args)
	{
		$args = array_slice($args, 1);
		$parsedArgs = array();
		foreach ($args as $arg) {
			if (strpos($arg, '--') !== false) {
				$arg = substr($arg, 2);
			}		
			
			$argAndValue = explode('=', $arg);
			$value = isset($argAndValue[1]) ? $argAndValue[1] : true;
			$parsedArgs[$argAndValue[0]] = $value;
		}
		return $parsedArgs;
	}
	
	public static function getLessonIndex($args)
	{
		$parsedArgs = self::getAgruments($args);
		if (!isset($parsedArgs[self::LESSON_PARAM_NAME])) {
			return '01';
		}
		return $parsedArgs[self::LESSON_PARAM_NAME];
	}
}