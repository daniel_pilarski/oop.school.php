<?php
ini_set('display_errors', true);
error_reporting(E_ALL & ~E_STRICT);

define('BASE_DIR', 'c:\xampp\htdocs\OOP.School');

set_include_path(BASE_DIR. PATH_SEPARATOR . get_include_path());
set_include_path(BASE_DIR.'\01.ObjectsInUse\\'. PATH_SEPARATOR . get_include_path());
set_include_path(BASE_DIR.'\01.PrivateProps\\'. PATH_SEPARATOR . get_include_path());
set_include_path(BASE_DIR.'\02.Inheritance\\' . PATH_SEPARATOR . get_include_path());
set_include_path(BASE_DIR.'\03.Interface.Abstract\\' . PATH_SEPARATOR . get_include_path());
set_include_path(BASE_DIR.'\04.Exceptions\\' . PATH_SEPARATOR . get_include_path());