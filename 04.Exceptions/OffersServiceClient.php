<?php
class OffersServiceClient {
	const FORBIDDEN_SELLER = '1000001';
	
	public function getSellerOffers($sellerId)
	{
		if ($sellerId == self::FORBIDDEN_SELLER) {
			throw new BlockedSellerException('Error! Can not fetch offers for seller: ' . $sellerId);
		}
		// Some REST API call.... service://offer-service/sellers/${sellerId}/offers
		return $this->getStubbedOffers($sellerId);
	}	
	
	public function getOffer($offerId)
	{
		// Some REST API call.... service://offer-service/offers/${offerId}
		return $this->getStubbedOffer($offerId);
	}
	
	private function getStubbedOffers($sellerId)
	{
		return array(
				new Offer(720111001, $sellerId, 'DELL XPS 15 9570 i7-8750H 32GB 512GB GF1050Ti FHD'),
				new Offer(720111002, $sellerId, 'DELL XPS 13 i7-6560U 8GB 256SSD QHD+ 10PRO NBD'),
				new Offer(720111002, $sellerId, 'MACBOOK PRO 15 RETINA i7 2.2GHz 16GB 256 MJLQ2ZE/A')
		);
	}
	
	private function getStubbedOffer($offerId)
	{
		return new Offer($offerId, '1000007', 'Rossignol narty Passion/Xpress W 10 165'); 
	}
	
}
