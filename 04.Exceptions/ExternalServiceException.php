<?php
class ExternalServiceException extends RuntimeException {
	public function __construct($message, $code = null)
	{
		parent::__construct($message, $code);
	}
}