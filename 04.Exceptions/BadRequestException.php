<?php
class BadRequestException extends HttpClientException {
	public function __construct($message, $code = null)
	{
		parent::__construct($message, $code);
	}
}