<?php
interface OffersRepository {
	function getOffers($sellerId);
	function getOffer($offerId);
}