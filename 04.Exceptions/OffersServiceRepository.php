<?php
class OffersServiceRepository implements OffersRepository {
	private $offersServiceClient;

	public function __construct(OffersServiceClient $offersServiceClient)
	{
		$this->offersServiceClient = $offersServiceClient;
	}

	public function getOffers($sellerId)
	{
		return $this->offersServiceClient->getSellerOffers($sellerId);
	}

	public function getOffer($offerId)
	{
		return $this->offersServiceClient->getOffer($offerId);
	}

}