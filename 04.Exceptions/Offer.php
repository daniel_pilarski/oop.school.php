<?php
class Offer {
	private $offerId;
	private $sellerId;
	private $name;
	
	public function __construct($offerId, $sellerId, $name)
	{
		$this->offerId = $offerId;
		$this->sellerId = $sellerId;
		$this->name;
	}
	
	public function getOfferId()
	{
		return $this->offerId;
	}
	
	public function getSellerId()
	{
		return $this->sellerId;
	}
	
	public function getName()
	{
		return $this->name;
	}
}