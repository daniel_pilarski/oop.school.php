<?php
class BlockedSellerException extends ExternalServiceException {
	public function __construct($message, $code = null)
	{
		parent::__construct($message, $code);
	}
}