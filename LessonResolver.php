<?php
class LessonResolver {

	public static function getLesson($index)
	{
		$lessons = self::availableLessons();
		if (!isset($lessons[$index])) {
			throw new RuntimeException('There is no lesson: '.$index);
		}
		return $lessons[$index];
	}

	private static function availableLessons()
	{
		return array(
				'1' => '01.ObjectsInUse',
				'begin' => '01.ObjectsInUse',
				'2' => '02.Inheritance',
				'inheritance' => '02.Inheritance',
				'3' => '03.Interface.Abstract',
				'interface' => '03.Interface.Abstract',
				'abstract' => '03.Interface.Abstract',
				'4' => '04.Exceptions',
				'exceptions' => '04.Exceptions'
		);
	}	
}