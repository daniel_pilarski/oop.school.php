<?php
require_once 'Person.php';

class Meeting {
// 	private $person1;
// 	private $person2;
	
	private $persons;
	private $personsCount;
	
// variant ze spotkaniem 2 os�b 	
// 	public function __construct(Person $person1, Person $person2)
// 	{
// 		$this->person1 = $person1;
// 		$this->person2 = $person2;
// 	}

	public function __construct(){
		$this->persons = array();
	}
	
	public function addPerson(Person $person)
	{
		$this->persons[] = $person;
	}
	
	public function greeting()
	{
// 		$this->person1->sayHello();
// 		echo PHP_EOL;
// 		$this->person2->sayHello();
// 		echo PHP_EOL;

		foreach ($this->persons as $person) {
			$person->sayHello(); 
			echo PHP_EOL;
		}
		
		echo "My name is Bond. James Bond! Dziadu!" . PHP_EOL ;
	}
}