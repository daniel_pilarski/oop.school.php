<?php
class Person {
	protected  $name;
	protected $surname;
	protected $age;
	
	public function __construct($name, $surname, $age)	
	{
		$this->name = $name; 
		$this->surname = $surname;
		$this->age = $age;
	}

	public function getName()
	{
		return $this->name;
	}
	
	public function getSurname()
	{
		return $this->surname;
	}

	public function getAge()
	{
		return $this->age;
	}
	
// 	public function setName($name)
// 	{
// 		$this->name = $name;
// 	}
	
	
	public function sayHello() 
	{
		printf("Hi, my name is %s %s. I'm %d yers old.", $this->getName(), $this->getSurname(), $this->getAge());
	}
	
// 	public function __destruct()
// 	{
// 		echo "Doing object destruction." . PHP_EOL;
// 	}
}