<?php
$michael = new Person('Michael', 'Jordan', 37);
$isiah = new Person('Isaiah', 'Kozber', 29);
$zygmunt = new Person('Zygmunt', 'Chrobry', 23);
$jan = new Person('Jan', 'Sobieski', 3);

// echo "Welcome of persons." . PHP_EOL;
// $michael->sayHello(); echo PHP_EOL;
// $isiah->sayHello(); echo PHP_EOL;

echo "Using Meeting object." . PHP_EOL;

// $meeting = new Meeting($michael, $isiah);
// Chc� �eby do spotkania doda� osoby: $michael, $isiah, $zygmunt, $jan 

$meeting = new Meeting();
$meeting->addPerson($michael);
$meeting->addPerson($isiah);
$meeting->addPerson($zygmunt);
$meeting->addPerson($jan);
$meeting->greeting();


// Zachcia�o mi si� doda� jescze jedn�;
$meeting->addPerson(new Person('Henryk', 'Ford', 31));
$meeting->greeting();


// unset($michael);
//var_dump($john);